# Overview #

Library for building and processing HTML forms with PHP or Python. Steals a lot from the Drupal form API. NodeJS version coming soon.

One of the most common functions of a web app is to receive user input, validate it, and then either show some errors and ask for more input, or do something cool and show a success message. For example, a contact form on a "Contact Us" page might ask for a name, e-mail address and message, make sure the fields are filled out, that the e-mail address looks somewhat sane, and then send an e-mail to support. This library makes it easier to define the form elements, the validation and submit actions, and then implement the form on the client-side.

# Requirements #

Obviously you're going to want to run some PHP 5, Python 2 or NodeJS 4 or later on the server side, and the client-side library uses JQuery 2 with the JQuery AJAX Form plugin/

#  Installation and Use #

Very straightforward. Take a look at the provided example for setup and usage.