
function formApi( formObj , ajaxOpts )
{
  this.formObj = formObj;
  this.submitBtn = $('input[type=submit]' , formObj ).eq(0);
  this.setupAjax( ajaxOpts );
}

formApi.prototype.setupAjax = function( ajaxOpts )
{
  ajaxOpts = typeof ajaxOpts == 'object' ? ajaxOpts : {};
  var successMessage = ajaxOpts.hasOwnProperty('success_message') ? ajaxOpts.success_message : '';
  var formApi = this;

  var defaultOpts = {
    success: function( response , status , xhr ){

      formApi.submitBtn.siblings('img.ajax-loader').remove();
      formApi.checkAjaxRedirect( response );
      var errors_exist = false;

      if( response.errors )
      {
        formApi.handleErrors( response.errors );
        errors_exist = true;
      }

      if( response.errorMessage )
      {
        formApi.errorMessage( response.errorMessage );
        errors_exist = true;
      }

      if( errors_exist )
        return;


      var resetForm = typeof response.updatedModel != 'undefined';

      if( response.successMessage )
        formApi.handleSuccess( response.successMessage , null , resetForm );      

      if( typeof ajaxOpts.on_success == 'function' ) 
        ajaxOpts.on_success( response , status , xhr );
    },
  
    beforeSend: function(){
      formApi.submitBtn.after('<img src="/images/ajax-loader.gif" class="ajax-loader"/>');
      formApi.resetErrors();
    },
    error: function(){
      formApi.submitBtn.closest('img.ajax-loader').remove();
      formApi.errorMessage('Whoops, there was an error. Please try again in a few minutes.');
    }
  };

  ajaxOpts = $.extend( {}  , defaultOpts , ajaxOpts );

  this.ajaxOpts = ajaxOpts;

  this.formObj.ajaxForm( ajaxOpts );
}

formApi.prototype.errorMessage = function( message )
{
  $('.form-item.submit', this.formObj ).after('<div class="message error">'+String(message)+'</div>');
}

formApi.prototype.handleErrors = function( errors , field_focus )
{
  var formObj = this.formObj;
  field_focus = typeof field_focus == 'boolean' ? field_focus : true;

  $.each( errors , function( field_id , error_message ){
    var errorMessage = '<div class="form-error-message"><div class="error-text">';
    errorMessage += String(error_message);
    errorMessage += '</div><div class="clearfix"></div></div>';

    var fieldObj = $('#'+field_id , formObj );

    fieldObj.addClass('error').bind('keyup.formapi',function(){
      $(this).closest('.form-item').removeClass('error').unbind('keyup.formapi');
    });
    fieldObj.closest('.form-item').append( errorMessage );
  });

  if( field_focus )
    $('.form-error-message', formObj ).eq(0).siblings('input,textarea').eq(0).focus();
}

formApi.prototype.resetErrors = function()
{
  $('.form-error-message' , this.formObj ).remove();
  $('input.error,textarea.error', this.formObj).removeClass('error');
  $('.message.error' , this.formObj ).remove();
}

formApi.prototype.reset = function()
{
  this.resetErrors();
  $('input:hidden, input:text, input:password, input:file, textarea', this.formObj).val('');
  $('select', this.formObj).prop('selectedIndex', 0);
  $('input:radio, input:checkbox', this.formObj).removeAttr('checked').removeAttr('selected');
}

formApi.prototype.handleSuccess = function( message , targetObj , resetForm )
{
  resetForm = typeof resetForm == 'boolean' ? resetForm : true;

  if( resetForm )
    this.reset();

  if( !targetObj )
    targetObj = this.submitBtn.closest('.form-item.submit');

  targetObj.after('<div class="message success">'+String(message)+'</div>');
  $('.message.success', this.formObj).delay(4000).fadeOut();
}

formApi.prototype.checkAjaxRedirect = function( response )
{
  if( typeof response == 'object' && response.redirect )
    window.location.href = response.redirect;
}  

formApi.prototype.mapModel = function( model , overrides )
{
  if(!model)
    return;

  var formObj = this.formObj;
  overrides = $.isArray(overrides) ? overrides : [];

  $.each( model , function( prop , val ){
    $('#' + prop , formObj ).val( val );
  });

  $.each( overrides , function( field_id , model_prop ){
    val = model[ model_prop ];
    $('#' + field_id , formObj ).val( val );
  });  
}
