<?php

class formapi
{
  protected $_post_data;
  protected $_form_struct;
  protected $_form_html;
  protected $_build_args;
  protected $_response_type = 'html';
  protected $_valid_fields = array('text','file','password','hidden','button','submit','textarea','select','multiselect','radio','radios','checkbox','markup');
  protected $_form_errors;
  protected $_field_success_messages;
  protected $_error_message = '';
  protected $_success_message = '';
  public $updated_model;
  public $store;

  public function __construct()
  {
    $this->store = (object)array();
  }

  public function errorMessage( $message )
  {
    $this->_error_message = $message;
  }

  public function successMessage( $message )
  {
    $this->_success_message = $message;
  }  

  public function error( $field_id , $message )
  {
    if(empty($this->_form_errors[ $field_id ]))
      $this->_form_errors[ $field_id ] = array();
    $this->_form_errors[ $field_id ][] = $message;
  }

  public function hasErrors()
  {
    return !empty($this->_form_errors) || !empty($this->_error_message);
  }  

  public function setErrors( $errors )
  {
    foreach( $errors as $field_id => $message )
    {
      $this->error( $field_id , $message );
    }
  }

  public function success( $field_id , $message ) 
  {
    if(empty($this->_field_success_messages[ $field_id ]))
      $this->_field_success_messages[ $field_id ] = array();
    $this->_field_success_messages[ $field_id ][] = $message;
  }

  public function get( $build_args = array() )
  {
    if(!method_exists( $this , 'form_build'))
      return '';

    if(empty($build_args['form_id']))
      $build_args['form_id'] = $this->_make_form_id();
    
    $this->_build_args = $build_args;

    if(!empty($_POST))
    {
      $this->_post_data = $_POST;

      if( $this->_check_submit_button() )
      {
        $this->_validate();

        if(!$this->hasErrors())
          $this->_submit();
      }
    }

    $this->_build_struct();

    $response_type = empty($this->_response_type) ? 'html' : $this->_response_type;

    switch( $response_type )
    {
      case 'html':
        $this->_render_html();
        return $this->_form_html;
      case 'json':
        $this->outputJson();
    }

  }

  public function outputJson()
  {
    $response = (object)array(
      'errors' => $this->_form_errors , 
      'errorMessage' => $this->_error_message,
      'success' => $this->_field_success_messages,
      'successMessage' => $this->_success_message,
      'updatedModel' => $this->updated_model
    );
    while(@ob_end_clean());
    header('Content-type: application/json');
    @session_write_close();
    echo utf8_encode( json_encode( $response ) );
    exit;
  }

  public function jsonResponse()
  {
    $this->_response_type = 'json';
  }

  private function _get_form_attr( $key )
  {
    return empty($this->_form_struct->attrs->$key) ? null : $this->_form_struct->attrs->$key;
  }

  
  /*
   * Do form validation on submission. We must call the form build function once here
   * in order to process any #required flags in field definitions
   */
  private function _validate()
  {
    $form_struct = $this->_get_form_struct();

    foreach( $form_struct as $field_key => $field )
    {
      if(empty($field['#required']))
        continue;

      if( $field['#required'] === true )
      {
        $field_id = empty($field['#id']) ? $field_key : $field['#id'];
        if(empty($this->_post_data[ $field_id ]))
          $this->error( $field_id , 'This field is required' );
      }
    }

    if(method_exists( $this , 'form_validate' ))
      $this->form_validate( $this->_post_data );
  }

  /*
   * Run the form submission function. Drupal likes to do a redirect here to prevent form
   * resubmission, but lets make that merely an optional convenience
   */
  private function _submit()
  {
    if(!method_exists( $this , 'form_submit'))
      return;

    $submit_args = array( $this, $this->_post_data );
    $this->form_submit( $this->_post_data );
  }


  /*
   * Did a submit button on this form trigger the POST? This is
   * handy when there are multiple forms on the same page
   */

  protected function _check_submit_button()
  {
    $form_struct = $this->_get_form_struct();

    foreach( $form_struct as $elem_id => $elem )
    {
      if(!empty($elem['#type']) && $elem['#type'] == 'submit' && isset($this->_post_data[ $elem_id ]))
        return true;
    }

    return false;
  }


  /*
   * Take the array the user passed and separate out attributes of the form and fields of the form 
   * into the form_struct attribute for ease of use later on
   */

  private function _build_struct()
  {
    $this->_form_struct = (object) array('attrs'=>(object)array(),'fields'=>array());

    $form_struct = $this->_get_form_struct();

    foreach( $form_struct as $key => $val )
    {
      if( substr( $key , 0 , 1 ) == '#' )
      {
        $key = substr($key,1);
        $this->_form_struct->attrs->$key = $val;
        continue;
      }

      $this->_form_struct->fields[ $key ] = $val;
    }

    $this->_form_struct->attrs->id = $this->_build_args['form_id'];
  }

  private function _get_form_struct()
  {
    if( method_exists( $this , 'form_build' ) )
      return $this->form_build( $this->_build_args );
    return array();
  }

  /*
   * Take the form_struct object generated previously and actually render html form elements
   */
  private function _render_html()
  {
    $html_elems = array();

    $action = $this->_get_form_attr('action') ? $this->_get_form_attr('action') : $_SERVER['REQUEST_URI'];
    $method = $this->_get_form_attr('method') ? strtoupper($this->_get_form_attr('method')) : 'POST';
    $method = $method == 'POST' ? 'POST' : 'GET';
    $attrs = $this->_get_form_attr('attributes') ? $this->_make_field_attrs( $this->_get_form_attr('attributes') ) : '';
    $form_id = $this->_get_form_attr('id');
    $write_error = false;
    $write_success = false;

    $html_elems[] = "<form action='{$action}' method='{$method}' id='{$form_id}' {$attrs}>";

    foreach( $this->_form_struct->fields as $field_key => $elem )
    {
      $field_type = empty($elem['#type']) ? 'text' : $elem['#type'];

      if(!in_array( $field_type , $this->_valid_fields ))
        continue;

      if( $field_type == 'markup' )
      {
        $html_elems[] = $elem['#value'];
        continue;
      }

      if(empty($elem['#id']))
        $elem['#id'] = $field_key;

      $render_func = '_render_field_html_'.$field_type;

      if( method_exists( $this , $render_func ) )
        $html_elems[] = $this->$render_func( $elem );

      if(!empty($this->_error_message) && !$write_error && $elem['#type'] == 'submit')
      {
        $write_error = true;
        $html_elems[] = "<div class='message error'>{$this->_error_message}</div>";
      }

      if(!empty($this->_success_message) && !$write_success && $elem['#type'] == 'submit')
      {
        $write_success = true;
        $html_elems[] = "<div class='message success'>{$this->_success_message}</div>";
      }      
    }

    $html_elems[] = "</form>";

    $this->_form_html = implode("\n",$html_elems);
  }

  private function _make_field_attrs( $attrs = array() )
  {
    $attr_html = '';
    foreach( $attrs as $key => $val )
    {
      $attr_html .= !empty($val) ? ($key.='="'.htmlentities($val,ENT_QUOTES).'"') : $key;
    }

    return $attr_html;
  }
 
  private function _get_field_errors( $elem_id )
  {
    $errors = (object)array('extra_class'=>'','html'=>'');
    if(empty($this->_form_errors[ $elem_id ]))
      return $errors;

    $error_msg = "<span class='error-sep'>";
    $error_msg = implode("</span><span class='error-sep'>, </span></span>"  , $this->_form_errors[ $elem_id ] );
    $error_msg .= "</span>";

    $errors->extra_class = ' error';
    $errors->html = "
     <div class='form-error-message'>
       <div class='error-text'>
         {$error_msg}
       </div>
       <div class='clearfix'></div>
     </div>
    ";

    return $errors;
  }  

  private function _get_field_success( $elem_id )
  {
    $successes = (object)array('extra_class'=>'','html'=>'');
    if(empty($this->_field_success_messages[ $elem_id ]))
      return $successes;

    $success_msg = "<span class='success-sep'>";
    $success_msg = implode("</span><span class='success-sep'>, </span></span>"  , $this->_field_success_messages[ $elem_id ] );
    $success_msg .= "</span>";

    $successes->extra_class = ' success';
    $successes->html = "
     <div class='form-success-message'>
       <div class='success-text'>
         {$success_msg}
       </div>
       <div class='clearfix'></div>
     </div>
    ";

    return $successes;
  }    

  private function _get_field_value( $elem )
  {
    switch( $elem['#type'] ) {
      case 'submit':
      case 'button':
        $field_val = isset($elem['#value']) ? $elem['#value'] : '';
      break;
      case 'multiselect':
        if(empty($elem['#default_value']))
          return array();

        return array_map( function( $a ){
          return htmlentities( $a , ENT_QUOTES );
        }, $elem['#default_value'] );
      break;
      default:
        $field_val = isset($elem['#default_value']) ? $elem['#default_value'] : '';
        if( !empty($this->_post_data[ $elem['#id'] ]) )
          $field_val = $this->_post_data[ $elem['#id'] ];
      break;
    }

    return htmlentities( $field_val , ENT_QUOTES );
  }

  private function _make_form_id()
  {
    $form_id = '';
    $classname = str_split(get_class( $this ));
     foreach( $classname as $letter )
     {
       if( $letter == ucfirst($letter) )
         $form_id .= '-';
       $form_id .= strtolower( $letter );
     }
     return $form_id;
  }



  private function _render_field_html_file( $elem )
  {
    $render_vars = $this->_make_render_vars( $elem );

     return "
       {$render_vars->field_prefix}
       <input type='file' id='{$render_vars->id}' name='{$render_vars->id}' />
       {$render_vars->field_suffix}
     ";
  } 

  private function _render_field_html_hidden( $elem )
  {
    $render_vars = $this->_make_render_vars( $elem );

     return "
       {$render_vars->field_prefix}
       <input type='hidden' id='{$render_vars->id}' name='{$render_vars->id}' value='{$render_vars->value}' />
       {$render_vars->field_suffix}
     ";
  }    

  private function _render_field_html_password( $elem )
  {
    return $this->_render_field_html_text( $elem , 'password' );
  }

  private function _render_field_html_text( $elem , $input_type = 'text' )
  {
    $render_vars = $this->_make_render_vars( $elem );

    if( $input_type == 'password' )
      $render_vars->value = '';

    $placeholder = empty($elem['#placeholder']) ? '' : 'placeholder="'.$elem['#placeholder'].'"';
    $max_length = isset($elem['#maxlength']) ? preg_replace('#[^0-9]+#','',$elem['#maxlength']) : 200;

     return "
      {$render_vars->prefix}
       <div class='form-item text {$render_vars->id}{$render_vars->errors->extra_class}'>
         <label class='form-item-label'>
           {$render_vars->title}
         </label>
         {$render_vars->field_prefix}
         <input type='{$input_type}' id='{$render_vars->id}' name='{$render_vars->id}' value='{$render_vars->value}' maxlength='{$max_length}' {$placeholder} {$render_vars->attrs} />
         {$render_vars->field_suffix}
         <div class='clearfix'></div>
         {$render_vars->errors->html}
         {$render_vars->successes->html}
       </div>
       {$render_vars->suffix}
     ";
  }

  private function _render_field_html_button( $elem )
  {
    return $this->_render_field_html_submit( $elem , 'button' );
  }

  private function _render_field_html_submit( $elem , $input_type = 'submit' )
  {
    $render_vars = $this->_make_render_vars( $elem );

     return "
      {$render_vars->prefix}
       <div class='form-item submit {$render_vars->id}'>
         <label class='form-item-label'>
           {$render_vars->title}
         </label>
         {$render_vars->field_prefix}
         <input type='{$input_type}' id='{$render_vars->id}' name='{$render_vars->id}' value='{$render_vars->value}' {$render_vars->attrs} />
         {$render_vars->field_suffix}
         <div class='clearfix'></div>
       </div>
       {$render_vars->suffix}
     ";
  }  

  private function _render_field_html_textarea( $elem )
  {
    $render_vars = $this->_make_render_vars( $elem );

     return "
      {$render_vars->prefix}
       <div class='form-item text {$render_vars->id}{$render_vars->errors->extra_class}'>
         <label class='form-item-label'>
           {$render_vars->title}
         </label>
         {$render_vars->field_prefix}
         <textarea id='{$render_vars->id}' name='{$render_vars->id}' {$render_vars->attrs}>{$render_vars->value}</textarea>
         {$render_vars->field_suffix}
         <div class='clearfix'></div>
         {$render_vars->errors->html}
         {$render_vars->successes->html}
       </div>
       {$render_vars->suffix}
     ";
  }  

  private function _render_field_html_radios( $elem )
  {
    $render_vars = $this->_make_render_vars( $elem );

     $html = "
      {$render_vars->prefix}
       <div class='form-item text {$render_vars->id}{$render_vars->errors->extra_class}'>
         <label class='form-item-label'>
           {$render_vars->title}
         </label>
         {$render_vars->field_prefix}";

      foreach( $elem['#options'] as $value => $label )
      {
        $checked = (!empty($elem['#default_value']) && $elem['#default_value'] == $value ) ? ' checked' : '';

        $html .= "
         <input type='radio' name='{$render_vars->id}'  value='{$value}'{$checked}/>
         <span class='radio-label'>{$label}</span>";
      }

      $html .= "
         <div class='clearfix'></div>
         {$render_vars->errors->html}
         {$render_vars->successes->html}
       </div>
       {$render_vars->suffix}
     ";

     return $html;
  }    

  private function _render_field_html_checkbox( $elem )
  {
    $render_vars = $this->_make_render_vars( $elem );

     return "
      {$render_vars->prefix}
       <div class='form-item text {$render_vars->id}{$render_vars->errors->extra_class}'>
         <label class='form-item-label'>
           {$render_vars->title}
         </label>
         {$render_vars->field_prefix}
         <input type='checkbox' id='{$render_vars->id}' name='{$render_vars->id}' {$render_vars->attrs} {$render_vars->checked} />
         <span class='checkbox-label'>{$render_vars->label}</span>
         <div class='clearfix'></div>
         {$render_vars->errors->html}
         {$render_vars->successes->html}
       </div>
       {$render_vars->suffix}
     ";
  }

  private function _render_field_html_multiselect( $elem )
  {
    return $this->_render_field_html_select( $elem , true );
  }    

  private function _render_field_html_select( $elem , $multi = false )
  {
    $render_vars = $this->_make_render_vars( $elem );
    $multi = $multi ? ' multiple' : '';
    $default_value = empty($elem['#default_value']) ? '' : $elem['#default_value'];

    $html = "
     {$render_vars->prefix}
     <div class='form-item text {$render_vars->id}{$render_vars->errors->extra_class}'>
       <label class='form-item-label'>
         {$render_vars->title}
       </label>
       {$render_vars->field_prefix}
       <select{$multi} id='{$render_vars->id}' name='{$render_vars->id}' {$render_vars->attrs}>";

    foreach( $elem['#options'] as $value => $label )
    {
      if(is_array($default_value))
        $selected = in_array( $value , $default_value ) ? ' SELECTED' : '';
      else
        $selected = $value == $default_value ? ' SELECTED' : '';

      $html .= "<option value='{$value}'{$selected}>{$label}</option>\n";
    }

    $html .= "
       </select>
       {$render_vars->field_suffix}
       <div class='clearfix'></div>
       {$render_vars->errors->html}
       {$render_vars->successes->html}       
     </div>
     {$render_vars->suffix}
    ";

    return $html;
  }  

  private function _make_render_vars( $elem )
  {
    $elem_id = $elem['#id'];
    
    return (object)array(
      'id' => $elem_id,
      'value' => $this->_get_field_value( $elem ),
      'attrs' => empty($elem['#attributes']) ? '' : $this->_make_field_attrs( $elem['#attributes'] ),
      'title' => empty($elem['#title']) ? '' : $elem['#title'],
      'label' => empty($elem['#label']) ? '' : $elem['#label'],
      'checked' => empty($elem['#checked']) ? '' : ' checked="checked"',
      'errors' => $this->_get_field_errors( $elem_id ),
      'successes' => $this->_get_field_success( $elem_id ),
      'field_prefix' => empty($elem['#field_prefix']) ? "" : "<span class='field-prefix'>{$elem['#field_prefix']}</span>",
      'field_suffix' => empty($elem['#field_suffix']) ? "" : "<span class='field-suffix'>{$elem['#field_suffix']}</span>",
      'prefix' => empty($elem['#prefix']) ? '' : $elem['#prefix'],
      'suffix' => empty($elem['#suffix']) ? '' : $elem['#suffix'],
    );
  }

}
