
import html

class formapi:
  __post_data = {}
  __form_struct = {}
  __form_html = ''
  __build_args = {}
  __response_type = 'html'
  __valid_fields = ('text','file','password','hidden','button','submit','textarea','select','multiselect','radio','radios','checkbox','markup')
  __form_errors = {}
  __field_success_messages = {}
  __error_message = ''
  __success_message = ''
  updated_model = {}
  store = None

  def errorMessage( self , message ):
    self.__error_message = message

  def successMessage( self , message ):
    self.__success_message = message

  def error( self , field_id , message ):
    if field_id not in self.__form_errors :
      self.__form_errors[ field_id ] = []

    self.__form_errors[ field_id ].append( message )

  def hasErrors( self ):
    return (len( self.__form_errors ) > 0 or len(self.__error_message) > 0)

  def setErrors( self , errors ):
    for key in errors.keys():
      self.error( key , errors[ key ] )

  def success( self , field_id , message ):
    if field_id not in self.__field_success_messages :
      self.__field_success_messages[ field_id ] = []

    self.__field_success_messages[ field_id ].append( message )

  def get( self , build_args = {} , post_data = {} ):

    ## no way to build the form? then there's nothing to do
    build_func = getattr( self , 'form_build' , None )
    if not callable(build_func):
      return ''

    if form_id not in build_args :
      build_args['form_id'] = self.__make_form_id()

    self.__build_args = build_args

    if len(post_data) > 0 :
      self.__post_data = post_data

      if self.__check_submit_button() :
        self.__validate()

        if not self.hasErrors():
          self.__submit()

    self.__build_struct()

    if self.__response_type == 'html':
      self.__render_html()
      return self.__form_html

    if self.__response_type == 'json':
      self.__outputJson()


  def outputJson( self ):
    response = {
      'errors': self.__form_errors,
      'errorMessage': self.__error_message,
      'success': self.__success_messages,
      'successMessage': self.__sucess_message,
      'updatedModel': self.updated_model
    }

    ## send json header
    ## send data
    ## exit


  def jsonResponse( self ):
    self.__response_type = 'json'


  def __get_form_attr( key ):
    return Null if key not in self.__form_struct['attrs'] else self.__form_struct['attrs'][ key ]


  def __validate( self ):
    form_struct = self.__get_form_struct()

    for key in form_struct.keys() :
      field = form_struct[ key ]
      if '#required' not in field:
        continue

      if field['#required'] == True:
        field_id = key if '#id' not in field else field['#id']
        if field_id not in self.__post_data or not self.__post_data[ field_id ]:
          self.error( field_id , 'This field is required' )

    validate_func = getattr( self , 'form_validate' , None )
    if callable( validate_func ):
      self.form_validate( self.__post_data )


  def __submit( self ):
    submit_func = getattr( self , 'form_submit' , None )
    if callable( submit_func ):
      self.form_submit( self.__post_data )

  def __check_submit_button( self ):
    form_struct = self.__get_form_struct()
    for key in form_struct.keys():
      field = self.__form_struct[ key ]
      if '#type' in field and field['#type'] is 'submit' and key in self.__post_data:
        return True
    return False


  def __build_struct( self ):
    self.__form_struct = { 'attrs': {} , 'fields': {} }
    form_struct = self.__get_form_struct()
    for key in form_struct.keys():
      if key[:1:1] == '#':
        key = key[1:]
        self.__form_struct['attrs'][ key ] = val
        continue

      self.__form_struct['fields'][ key ] = val;

    self.__form_struct['attrs']['id'] = self.__build_args['form_id']


  def __get_form_struct( self ):
    build_func = getattr( self , 'form_build' , None )
    if callable( build_func ):
      return self.form_build( self.__build_args )
    return {}


  def __render_html( self ):
    html_elems = []

    action = self.__get_form_attr('action')
    method = self.__get_form_attr('method')
    attrs = self.__get_form_attr('attributes')
    if len(attrs) > 0:
      attrs = self.__make_field_attrs( attrs )
    form_id = self.__get_form_attr('id')
    write_error = False
    write_success = False

    html_elems.append("<form action='{action}' method='{method}' id='{form_id}' {attrs}>".format( action=action ,
         method=method , id=form_id, attrs=attrs
      ))

    for field_key in self.__form_struct['fields']:
      elem = self.__form_struct['fields'][ field_key ]
      field_type = elem['#type'] if '#type' in elem else 'text'

      if field_type not in self.__valid_fields:
        continue

      if field_type == 'markup':
        html_elems.append( elem['#value'] )
        continue

      if '#id' not in elem or len(elem['#id']) < 1:
        elem['#id'] = field_key

      render_func = getattr( self  , '__render_field_html_' + field_type , None )

      if callable( render_func ):
        html_elems.append( render_func( elem ) )

      if len(self.__error_message) > 0 and not write_error and field_type == 'submit':
        write_error = True
        html_elems.append( "<div class='message error'>{error_message}</div>".format(error_message=self.__error_message) )

      if len(self.__success_message) > 0 and not write_success and field_type == 'submit':
        write_success = True
        html_elems.append("<div class='message success'>{success_message}</div>".format(success_message=self.__success_message))

    html_elems.append("</form>")

    self.__form_html = "\n".join( html_elems )


  def __make_field_attrs( self , attrs ):
    attr_html = ''
    for key in attrs:
      val = attrs[ key ]
      attr_html += ('key="' + html.escape( val ) + '"') if len(val) > 0 else key

    return attr_html


  def __get_field_errors( self , elem_id ):
    response = {'extra_class': '','html': ''}

    if elem_id not in self.__form_errors:
      return response

    message = '<span class="error-sep">'
    message += '</span><span class="error-sep">, </span></span>'.join( self.__form_errors[ elem_id ] )
    message += '</span>'

    response['extra_class'] = ' error'

    response['html'] = """
     <div class="form-error-message">
       <div class="error-text">
         {message}
       </div>
       <div class="clearfix"></div>
     </div>      
    """.format(message=message)

    return response


  def __get_field_success( self , elem_id ):
    response = {'extra_class': '','html': ''}

    if elem_id not in self.__field_success_message:
      return response

    message = '<span class="success-sep">'
    message += '</span><span class="success-sep">, </span></span>'.join( self.__field_success_messages[ elem_id ] )
    message += '</span>'

    response['extra_class'] = ' success'

    response['html'] = """
     <div class="form-success-message">
       <div class="success-text">
         {message}
       </div>
       <div class="clearfix"></div>
     </div>      
    """.format(message=message)

    return response


  def __get_field_value( self , elem ):
    elem_type = elem['#type'];

    if elem_type == 'submit' or elem_type == 'button':
      field_val = elem['#value'] if '#value' in elem else ''
      return field_val

    if elem_type == 'multiselect':
      if '#default_value' not in elem:
        return {}
      
      return map( html.escape , elem['#default_value'] )

    field_val = elem['#default_value'] if '#default_value' in elem else ''

    return html.escape( field_val )


  def __make_form_id( self ):
    form_id = ''
    class_name = self.__class__.__name__
    for ltr in class_name:
      if ltr == ltr.upper():
        form_id += '-'
      form_id += ltr.lower()

    return form_id


  def __make_render_vars( self , elem ):

    elem_id = elem['#id']

    return {
     'id': elem_id,
     'value' : self.__get_field_value( elem ),
     'attrs' : self.__make_field_attr( elem['#attributes'] ) if '#attributes' in elem else '',
     'title' : elem['#title'] if '#title' in elem else '',
     'label' : elem['#label'] if '#label' in elem else '',
     'checked' : 'checked="checked"' if '#checked' in elem else '',
     'errors' : self.__get_field_errors( elem_id ),
     'successes' : self.__get_field_success( elem_id ),
     'field_prefix' : '<span class="field-prefix">' + elem['#field_prefix'] + '</span>' if '#field_prefix' in elem else '',
     'field_suffix' : '<span class="field-suffix">' + elem['#field_suffix'] + '</span>' if '#field_suffix' in elem else '',
     'prefix' : elem['#prefix'] if '#prefix' in elem else '',
     'suffix' : elem['#suffix'] if '#suffix' in elem else ''
    }


  def __render_field_html_file( self , elem ):
    render_vars = self.__make_render_vars( elem )

    return """
       {field_prefix}
       <input type='file' id='{field_id}' name='{field_id}' />
       {field_suffix}
        """.format( 
          field_prefix=render_vars['field_prefix'] , 
          field_suffix=renders_vars['field_suffix'],
          field_id=render_vars['id']
          )

  def __render_field_html_password( self , elem ):
    return self.__render_field_html_text( elem , 'password' )

  def __render_field_html_text( self , elem , input_type = 'text' ):

    render_vars = self.__make_render_vars( elem )

    if input_type == 'password':
      render_vars['value'] = ''

    placeholder = elem['#placeholder'] if '#placeholder' in elem else ''
    max_length = elem['#maxlength'] if '#maxlength' in elem else ''

    return """
      {prefix}
       <div class='form-item text {field_id}{extra_class}'>
         <label class='form-item-label'>
           {title}
         </label>
         {field_prefix}
         <input type='{input_type}' id='{field_id}' name='{field_id}' value='{value}' maxlength='{max_length}' {placeholder} {attrs} />
         {field_suffix}
         <div class='clearfix'></div>
         {error_html}
         {success_html}
       </div>
       {suffix}
     """.format(
      prefix=render_vars['prefix'],
      suffix=render_vars['suffix'],
      field_id=render_vars['id'],
      extra_class=render_vars['errors']['extra_class'],
      title=render_vars['title'],
      field_prefix=render_vars['field_prefix'],
      field_suffix=render_vars['field_suffix'],
      input_type=input_type,
      value=render_vars['value'],
      max_length=max_length,
      placeholder=placeholder,
      attrs=render_vars['attrs'],
      error_html=render_vars['errors']['html'],
      success_html=render_vars['successes']['html']
      )

  def __render_field_html_submit( self ):
    return self.__render_field_html_button( elem , 'submit' )

  def __render_field_html_button( self , elem , input_type = 'button' ):
    
    render_vars = self.__make_render_vars( elem )   

    return """
      {prefix}
       <div class='form-item submit {field_id}'>
         <label class='form-item-label'>
           {title}
         </label>
         {field_prefix}
         <input type='{input_type}' id='{field_id}' name='{field_id}' value='{value}' {attrs} />
         {field_suffix}
         <div class='clearfix'></div>
       </div>
       {suffix}
     """.format(
      input_type=input_type,
      prefix=render_vars['prefix'],
      suffix=render_vars['suffix'],
      field_id=render_vars['id'],
      title=render_vars['title'],
      field_prefix=render_vars['field_prefix'],
      field_suffix=render_vars['field_suffix'],
      value=render_vars['value'],
      attrs=render_vars['attrs'],
      )


  def __render_field_html_textarea( self , elem ):

    render_vars = self.__make_render_vars( elem )   

    return """
      {prefix}
       <div class='form-item text {field_id}{extra_class}'>
         <label class='form-item-label'>
           {title}
         </label>
         {field_prefix}
         <textarea id='{field_id}' name='{field_id}' {attrs}>{value}</textarea>
         {field_suffix}
         <div class='clearfix'></div>
         {error_html}
         {success_html}
       </div>
       {suffix}
      """.format(
        prefix=render_vars['prefix'],
        suffix=render_vars['suffix'],
        field_id=render_vars['id'],
        extra_class=render_vars['errors']['extra_class'],
        title=render_vars['title'],
        field_prefix=render_vars['field_prefix'],
        value=render_vars['value'],
        attrs=render_vars['attrs'],
        field_suffix=render_vars['field_suffix'],
        error_html=render_vars['errors']['html'],
        success_html=render_vars['successes']['html']
        )


  def __render_field_html_radios( self , elem ):

    render_vars = self.__make_render_vars( elem )

    html = """
      {prefix}
       <div class='form-item text {field_id}{extra_class}'>
         <label class='form-item-label'>
           {title}
         </label>
         {field_prefix}
      """.join(
       prefix=render_vars['prefix'],
       field_id=render_vars['id'],
       extra_class=render_vars['errors']['extra_class'],
       title=render_vars['title'],
       field_prefix=render_vars['field_prefix'],
       )

    for value in elem['#options'].keys():
      label = elem['#options'][ value ]
      checked = ' checked' if '#default_value' in elem and elem['#default_value'] == value else ''

      html += """
         <input type='radio' name='{field_id}'  value='{value}'{checked}/>
         <span class='radio-label'>{label}</span>
        """.join(
          field_id=render_vars['id'],
          value=value,
          checked=checked,
          label=label
        )

      html += """
         <div class='clearfix'></div>
         {error_html}
         {success_html}
       </div>
       {suffix}
      """.join(
        suffix=render_vars['suffix'],
        error_html=render_vars['errors']['html'],
        success_html=render_vars['successes']['html']
        )

      return html


  def __render_field_html_checkbox( self , elem ):

    render_vars = self.__make_render_vars( elem )

    return """
      {prefix}
       <div class='form-item text {field_id}{extra_class}'>
         <label class='form-item-label'>
           {title}
         </label>
         {field_prefix}
         <input type='checkbox' id='{field_id}' name='{field_id}' {attrs} {checked} />
         <span class='checkbox-label'>{label}</span>
         <div class='clearfix'></div>
         {error_html}
         {success_html}
       </div>
       {suffix}
     """.format(
      prefix=render_vars['prefix'],
      suffix=render_vars['suffix'],
      field_id=render_vars['id'],
      extra_class=render_vars['errors']['extra_class'],
      title=render_vars['title'],
      field_prefix=render_vars['field_prefix'],
      value=render_vars['value'],
      attrs=render_vars['attrs'],
      checked=render_vars['checked'],
      label=render_vars['label'],
      error_html=render_vars['errors']['html'],
      success_html=render_vars['successes']['html']
      ) 


  def __render_field_html_multiselect( self , elem ):
    return self.__render_field_html_select( elem , True )


  def __render_field_html_select( self , elem , multi = False ):

    render_vars = self.__make_render_vars( elem )
    multi = ' multiple' if multi else ''
    default_value = elem['#default_value'] if '#default_value' in elem else ''

    html = """
     {prefix}
     <div class='form-item text {field_id}{extra_class}'>
       <label class='form-item-label'>
         {title}
       </label>
       {field_prefix}
       <select{multi} id='{field_id}' name='{field_id}' {attrs}>
      """.format(
        prefix=render_vars['prefix'],
        field_id=render_vars['id'],
        extra_class=render_vars['errors']['extra_class'],
        title=render_vars['title'],
        field_prefix=render_vars['field_prefix'],
        multi=multi,
        attrs=render_vars['attrs'],
        )

    for value in elem['#options']:
      label = elem['#options'][ value ]

      if type( default_value ) == 'dict':
        selected = ' selected' if value in default_value.values() else ''
      else:
        select = ' selected' if value == default_value else ''

      html += "<option value='{value}'{selected}>{label}</option>\n".format(value=value,label=label,selected=selected)

    html += """
         </select>
         {field_suffix}
         <div class='clearfix'></div>
         {error_html}
         {success_html}       
       </div>
       {suffix}
      """.format(
        field_suffix=render_vars['field_prefix'],
        error_html=render_vars['errors']['html'],
        success_html=render_vars['successes']['html']
        )        