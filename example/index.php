<?php

require '../formapi.php';

class contactForm extends formapi
{
  function form_build()
  {
    $form = array();

    $form['name'] = array(
     '#title' => 'Name',
     '#type' => 'text'
    );

    $form['email'] = array(
     '#title' => 'E-mail',
     '#type' => 'text'
    );

    $form['message'] = array(
     '#title' => 'Message',
     '#type' => 'textarea'
    );  

    $form['send-message'] = array(
     '#value' => 'Send',
     '#type' => 'submit'
    );

    return $form;
  }

  function form_validate( $data )
  {
    if(empty($data['name']))
      $this->error('name','Please enter your name');

    if(empty($data['email']))
      $this->error('email','Please enter your e-mail address');    
  }

  function form_submit( $data )
  {
    // the form was submitted and passed validation, maybe send an e-mail?

    $this->successMessage('Your message was sent.');
  }
}

$form = new contactForm();

if(!empty($_POST))
{
  $form->jsonResponse();
  $form->get();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>PHP/Python Form API Example</title>
  <meta charset="utf-8">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../formapi.js"></script>
  <script src="jquery.form.min.js"></script>
  <style type='text/css'>
  label.form-item-label{ display: inline-block; width: 75px; }
  .form-item{ margin-bottom: 20px; }
  .error-text{ color: #990000; font-weight: bold; margin: 5px 0px 5px 80px; }
  .message.success{ margin-left: 80px; color: #009900; font-weight: bold; }
  </style>
</head>
<body>

<?php echo $form->get(); ?>

<script>

$(function(){

  var contactForm = new formApi( $('form#contact-form') );

});

</script>
</body>
</html>